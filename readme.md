# TinyPNG API  Web Interface

This creates a website in which to use the TinyPNG API. It allows you to input URLs and have the file structure remain the same for the compressed images.

### Purpose

This to solve the problem of reducing many large images and then having to painstakingly upload the compressed images one-by-one.

This system allows you to enter URLs of images, and then saves the compressed images with the same file structure.

These can then upload all the compressed and resized images in one pass.

### Prerequisites

Follow the guide to install the TinyApi PHP code using composer
[https://tinypng.com/developers/reference/php]

### Installing

You need to copy /TinyApp/config.php.example to /TinyApp/config.php and update it with your own TinyPNG API Token.

Once that's done you will need to run composer dump-autoload -o to add our classes to your autoloader.

### Usage

To use, enter some URLs or file paths to images on the resize page. The system will use TinyPNG's API to reduce the file weight, and optionally to reduce the dimentions.

## Built With

* [TinyPNG](https://tinypng.com/developers/reference/php) - Image compression
* [Tinify](https://packagist.org/packages/tinify/tinify) - Tinify Composer Package
* [Bootstrap](https://getbootstrap.com/) - CSS / JS

## Authors

* **Liam Delahunty** - [OnlineSales](https://www.onlinesales.co.uk/)

## License

This project is licensed under the MIT License.

