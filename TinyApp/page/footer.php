<footer class="footer mt-4 pt-4" style="text-align:center; border-top:1px #ccc solid;">

<?php include INC_ROOT . '/TinyApp/page/top-nav.php';?>

    <p class="text-muted">&copy; Made with love by <a href="http://www.onlinesales.co.uk/" target="_blank">OnlineSales Webmaster Tools</a> </p>
</footer>
