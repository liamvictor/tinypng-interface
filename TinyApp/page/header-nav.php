    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="<?=INSTALL_PATH;?>">TinyPNG Interface</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="./" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tools</a>
              <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="<?=INSTALL_PATH;?>tiny-resize.php">Resize &amp; Reduce Images</a>
                <a class="dropdown-item" href="<?=INSTALL_PATH;?>img-data.php">Img Data</a>
                <a class="dropdown-item" href="<?=INSTALL_PATH;?>local-exists.php">Local Exists</a>
                <a class="dropdown-item" href="<?=INSTALL_PATH;?>zip-dir.php">Zip</a>
                <a class="dropdown-item" href="<?=INSTALL_PATH;?>about.php">About</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </header>
