<?php

echo "<div class=\"row\"><div class=\"col-xs-12\">";
echo "<table class=\"table table-striped\"><thead>";
echo "<tr><th>";
/*
|--------------------------------------------------------------------------
| so clever
|--------------------------------------------------------------------------
 */
echo implode('</th><th>', array_keys(current($array)));
echo "</tr></thead>" . PHP_EOL;
echo "<tbody>";

/*
|--------------------------------------------------------------------------
| Brilliant
|--------------------------------------------------------------------------
 */
foreach ($array as $row): array_map('htmlentities', $row);
    echo "<tr><td>";
    echo implode('</td><td>', $row);
    echo "</td></tr>";
endforeach;
echo "</tbody></table>" . PHP_EOL;
echo "</div><!-- col --></div><!-- row -->" . PHP_EOL;
