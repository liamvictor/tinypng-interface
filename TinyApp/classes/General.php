<?php

namespace TinyApp;

class General
{
    public static function makeArrayAndClean($myArray)
    {
        $myArray = preg_replace("/\r\n|\r|\n/", PHP_EOL, strip_tags(trim($myArray))); // fixes different flavours of line breaks. I hope.
        $myArray = explode(PHP_EOL, $myArray);
        // remove blank lines
        foreach ($myArray as $in) {
            $in = trim($in); // becuase of tabs in cutting and pasting lines from OO Calc
            if (!empty($in)) {
                $out[] = $in;
            }
        }
        $out = array_unique($out);
        return ($out);
    }

    /*
     * PHP Recursive Backup-Script to ZIP-File
     * (c) 2012: Marvin Menzerath. (http://menzerath.eu)
     */
    public function zipData($source, $destination)
    {
        if (extension_loaded('zip') === true) {
            if (file_exists($source) === true) {
                $zip = new \ZipArchive();
                if ($zip->open($destination, \ZIPARCHIVE::CREATE) === true) {
                    $source = realpath($source);
                    if (is_dir($source) === true) {
                        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
                        foreach ($files as $file) {
                            $file = realpath($file);
                            if (is_dir($file) === true) {
                                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                            } else if (is_file($file) === true) {
                                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                            }
                        }
                    } else if (is_file($source) === true) {
                        $zip->addFromString(basename($source), file_get_contents($source));
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }
}
