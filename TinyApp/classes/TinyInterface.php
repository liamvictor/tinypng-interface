<?php

namespace TinyApp;

class TinyInterface
{

    public $response;

    public function __construct(Img $img)
    {

        try {

            $tiny = new \Tinify\Tinify();
            $tiny->setKey(TNYTOKEN);
            $this->ResizeImage($img);

        } catch (\Tinify\AccountException $e) {
            // Verify your API key and account limit.a
            $img->error[] = "Verify your API key and account limit. The error message is: " . $e->getMessage();
        } catch (InputException $e) {
            // Not a valid PNG or JPEG
            $img->error[] = "Not a valid PNG or JPEG. The error message is: " . $e->getMessage();
        } catch (\Tinify\ClientException $e) {
            // Check your source image and request options.
            $img->error[] = "Check your source image and request options. The error message is: " . $e->getMessage();
        } catch (\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            $img->error[] = "Temporary issue with the Tinify API. The error message is: " . $e->getMessage();
        } catch (\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            $img->error[] = "A network connection error occurred. The error message is: " . $e->getMessage();
        } catch (Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            $img->error[] = "Something else went wrong, unrelated to the Tinify API. The error message is: " . $e->getMessage();
        }
        return;
    }

    public function ResizeImage(Img $img)
    {

        if (filter_var($img->path, FILTER_VALIDATE_URL)) {
            // URLs

            if (!empty($img->resizewidth)) {

                $source  = \Tinify\fromUrl($img->path);
                $resized = $source->resize(array(
                    "method" => $img->resizemethod,
                    "width"  => (int) $img->resizewidth,
                ));

                $this->response = $resized->toFile($img->full_local_path);

            } else {

                $this->response = \Tinify\fromUrl($img->path)->toFile($img->full_local_path);

            }

        } else {
            // local source files

            if (!empty($img->resizewidth)) {

                $source  = \Tinify\fromFile($img->path);
                $resized = $source->resize(array(
                    "method" => $img->resizemethod,
                    "width"  => (int) $img->resizewidth,
                ));

                $this->response = $resized->toFile($img->full_local_path);

            } else {

                $this->response = \Tinify\fromFile($img->path)->toFile($img->full_local_path);
            }
        }

        if (!empty($this->response)) {
            $img->setResponse($this->response);
        }

        $compressions_this_month = \Tinify\compressionCount();
        if (!empty($compressions_this_month)) {
            $img->setCompressionsThisMonth($compressions_this_month);
        }

        $img->setProcessed();
        $img->setProcessedCount();

    }

}
