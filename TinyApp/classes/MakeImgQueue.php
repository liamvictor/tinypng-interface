<?php

namespace TinyApp;

class MakeImgQueue extends Img
{
    public $queue_item;

    public function __construct($imgs)
    {
        foreach ($imgs as $img) {
            if (empty($img->file_exists)) {
                // all fresh
                $this->queue_item[] = $img;
            } elseif (!empty($img->file_exists) && !empty($img->skip_existing_local) && !empty($img->overwrite_local_files)) {
                // file exists. skip it.
            } elseif (!empty($img->file_exists) && empty($img->skip_existing_local)) {
                // file exists. Don't skip it.
                $this->queue_item[] = $img;
            } elseif (!empty($img->file_exists) && !empty($img->overwrite_local_files)) {
                // File exists, overwrite it.
                $this->queue_item[] = $img;
            }
        }
        return $this;
    }

} // class Img
