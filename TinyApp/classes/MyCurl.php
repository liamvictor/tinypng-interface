<?php

namespace TinyApp;

class MyCurl
{
    /*
    |--------------------------------------------------------------------------
    | Just talking about MyCurl. ♪♬
    |--------------------------------------------------------------------------
     */
    public $remotefile;
    public $ch;
    public $data;
    public $error;
    public $status;
    public $contentLength;

    public function __construct($url)
    {
        $this->remotefile = $url;
        $this->ch         = curl_init($this->remotefile);
        $this->getCurl();
    }

    public function getCurl()
    {
        curl_setopt($this->ch, CURLOPT_NOBODY, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HEADER, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true); // not necessary unless the file redirects (like the PHP example we're using here)

        $this->data = curl_exec($this->ch);
        curl_close($this->ch);
        if ($this->data === false) {
            $this->error = 'cURL failed';
            return;
        }
        $this->getLength();
    }

    public function getLength()
    {
        # code...

        if (preg_match('/^HTTP\/1\.[01] (\d\d\d)/', $this->data, $matches)) {
            $this->status = (int) $matches[1];
        }
        if (preg_match('/Content-Length: (\d+)/', $this->data, $matches)) {
            $this->contentLength = (int) $matches[1];
        }
        return [$this->status, $this->contentLength];
    }
}
