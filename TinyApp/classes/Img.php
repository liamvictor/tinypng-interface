<?php

namespace TinyApp;

class Img
{
    public $path;
    public $fqdn;
    public $dir;
    public $host;
    public $error;
    public $filename;
    public $extension;
    public $basename;
    public $remotefilesize;
    public $n;
    public $nfmt;
    public $outpath;
    public $local_basename;
    public $used_local_basename;
    public $full_local_path;
    public $existing_local_path;
    public $localfilesize;
    public $overwrite_local_files;
    public $skip_existing_local;
    public $file_exists;
    // public $tinyPngToken;
    public $tiny;
    public $processed;
    public $processed_count;
    public $response;
    public $resizewidth;
    public $resizemethod;
    public $compressions_this_month;

    // methods
    public function __construct($path, $resizemethod = null, $resizewidth = null, $overwrite_local_files = true, $skip_existing_local = true, $skip_remote_test = false)
    {

        $this->path = $path;
        $this->n    = 0;
        try {

            if (!empty($skip_remote_test)) {

                $headers = get_headers($this->path, 1);
                // https://www.brlets.co.uk/news/wp-content/uploads/2017/06/104-Ariel-House-London-Dock-4.jpg
                // array (size=13)
                //   0 => string 'HTTP/1.1 200 OK' (length=15)
                //   'Date' => string 'Thu, 12 Jul 2018 09:46:37 GMT' (length=29)
                //   'Server' => string 'Apache' (length=6)
                //   'Last-Modified' => string 'Fri, 18 May 2018 06:15:04 GMT' (length=29)
                //   'Accept-Ranges' => string 'bytes' (length=5)
                //   'Content-Length' => string '6264993' (length=7)
                //   'Cache-Control' => string 'max-age=604800, public' (length=22)
                //   'Expires' => string 'Sat, 11 Aug 2018 09:46:37 GMT' (length=29)
                //   'Strict-Transport-Security' => string 'max-age=31536000' (length=16)
                //   'Vary' => string 'User-Agent' (length=10)
                //   'Connection' => string 'keep-alive, close' (length=17)
                //   'Keep-Alive' => string 'timeout=100,max=500' (length=19)
                //   'Content-Type' => string 'image/jpeg' (length=10)

                // will redirect - say for http to https.
                // so get_headers could be an array
                // array (size=15)
                //   0 => string 'HTTP/1.1 301 Moved Permanently' (length=30)
                //   'Date' =>
                //     array (size=2)
                //       0 => string 'Thu, 12 Jul 2018 09:48:39 GMT' (length=29)
                //       1 => string 'Thu, 12 Jul 2018 09:48:39 GMT' (length=29)
                //   'Server' =>
                //     array (size=2)
                //       0 => string 'Apache' (length=6)
                //       1 => string 'Apache' (length=6)
                //   'Location' => string 'https://www.brlets.co.uk/news/wp-content/uploads/2017/06/104-Ariel-House-London-Dock-4.jpg' (length=90)
                //   'Cache-Control' =>
                //     array (size=2)
                //       0 => string 'max-age=864000' (length=14)
                //       1 => string 'max-age=604800, public' (length=22)
                //   'Expires' =>
                //     array (size=2)
                //       0 => string 'Sun, 22 Jul 2018 09:48:39 GMT' (length=29)
                //       1 => string 'Sat, 11 Aug 2018 09:48:39 GMT' (length=29)
                //   'Content-Length' =>
                //     array (size=2)
                //       0 => string '298' (length=3)
                //       1 => string '6264993' (length=7)
                //   'Connection' =>
                //     array (size=2)
                //       0 => string 'close' (length=5)
                //       1 => string 'keep-alive, close' (length=17)
                //   'Content-Type' =>
                //     array (size=2)
                //       0 => string 'text/html; charset=iso-8859-1' (length=29)
                //       1 => string 'image/jpeg' (length=10)
                //   1 => string 'HTTP/1.1 200 OK' (length=15)
                //   'Last-Modified' => string 'Fri, 18 May 2018 06:15:04 GMT' (length=29)
                //   'Accept-Ranges' => string 'bytes' (length=5)
                //   'Strict-Transport-Security' => string 'max-age=31536000' (length=16)
                //   'Vary' => string 'User-Agent' (length=10)
                //   'Keep-Alive' => string 'timeout=100,max=500' (length=19)

                $this->setHeaders($headers);
                if (!array_key_exists('Content-Length', $headers)) {
                    throw new \Exception("No Content-Length! File not found? ");
                } else {
                    $this->setRemoteFileSize($headers['Content-Length']);
                }
            }

            $pathinfo = pathinfo($path);
            // array (size=4)
            //   'dirname' => string 'https://www.brlets.co.uk/news/wp-content/uploads/2017/06' (length=56)
            //   'basename' => string '104-Ariel-House-London-Dock-4.jpg' (length=33)
            //   'extension' => string 'jpg' (length=3)
            //   'filename' => string '104-Ariel-House-London-Dock-4' (length=29)=/home/vagrant/Code/tinyapi/TinyApp/classes/ImgHTML.php:48:
            $parsed_url = parse_url($path);
            // array (size=3)
            //   'scheme' => string 'https' (length=5)
            //   'host' => string 'www.brlets.co.uk' (length=16)
            //   'path' => string '/news/wp-content/uploads/2017/06/104-Ariel-House-London-Dock-4.jpg' (length=66)

            $this->setFQDN($parsed_url);

            if (!array_key_exists('path', $parsed_url)) {
                throw new \Exception("No path!");
            } else {
                $this->setDir($parsed_url['path']);
            }
            if (!array_key_exists('basename', $pathinfo)) {
                throw new \Exception("No basename - $path");
            } else {
                $this->setBasename($pathinfo['basename']); // file.ext
            }
            if (!array_key_exists('extension', $pathinfo)) {
                throw new \Exception("Path hasn't an extension - $path");
            } else {
                $this->setExtension($pathinfo['extension']); // ext
            }
            if (!array_key_exists('filename', $pathinfo)) {
                throw new \Exception("No filename - $path");
            } else {
                $this->setFilename($pathinfo['filename']); // file - no extension
            }

            if (!array_key_exists('host', $parsed_url)) {
                throw new \Exception("No host");
            } else {
                $this->setHost($parsed_url); // domain name including subdomain
            }

            if (!empty($skip_existing_local)) {
                $this->skip_existing_local = $skip_existing_local;
            }

            if (!empty($overwrite_local_files)) {
                $this->overwrite_local_files = $overwrite_local_files;
            }

            $this->setOutpath();
            // $this->setTinyPngToken();

            if (!empty($resizemethod)) {
                $this->setResizeMethod($resizemethod);
            }

            if (!empty($resizewidth)) {
                $this->setResizeWidth($resizewidth);
            }

            // getting the remote size will slow down the script.
            // $this->setRemoteImgSize($path);
            // can we get it from the Tinyify afterwards?

            // setFullLocalPath has to be last.
            $this->setFullLocalPath($this->basename);

        } catch (\Exception $e) {
            $this->error[] = $e->getMessage();
        }
    }

    public function hasErrors()
    {
        if (!empty($this->error)) {
            return true;
        }
    }

    public function setBasename($basename)
    {
        $basename = preg_replace('/\?.*/', '', $basename);
        if (!empty($basename)) {
            $this->basename = $basename;
            $this->setLocalBasename($basename);
        }
    }

    public function setDir($path)
    {
        $this->dir = dirname($path);
    }

    public function setExtension($extension)
    {
        $extension       = preg_replace('/\?.*/', '', $extension);
        $this->extension = $extension;
    }

    public function setFilename($filename)
    {
        // no extension
        $this->filename = $filename;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function setLocalBasename($local_basename)
    {
        // initially created the same as remote basename.
        // Updated if the file exists.
        // basename has extension, filename doesn't.
        // ¯\_(ツ)_/¯ I don't make the rules.
        $this->local_basename = $local_basename;
    }

    public function setFileExists()
    {
        $this->file_exists = true;
    }

    public function setFQDN($parsed_url)
    {

        if (empty($parsed_url['scheme'])) {
            $this->fqdn = null;
            return;
        }

        if (empty($parsed_url['host'])) {
            $this->fqdn = null;
            return;
        }

        $this->fqdn = $parsed_url['scheme'] . "://" . $parsed_url['host'];
    }

    public function setHost($parsed_url)
    {
        if (empty($parsed_url['host'])) {
            $this->host = null;
            return;
        }
        $this->host = $parsed_url['host'];
    }

    public function setOutpath()
    {
        if (!empty($this->host)) {
            $this->outpath = OUTPATH_ROOT . $this->host;
        } else {
            $this->outpath = OUTPATH_ROOT;
        }
    }

    public function setProcessed()
    {
        $this->processed[] = time();
    }

    public function setProcessedCount()
    {
        $this->processed_count++;
    }

    public function setResponse($response)
    {
        // This is the filesize returned by Tinify.
        $this->response = $response;
        $this->setLocalFileSize($response);

    }

    public function getUsedLocalBasename()
    {
        if (empty($this->used_local_basename)) {
            return $this->local_basename;
        }
        return $this->used_local_basename;
    }

    public function setUsedLocalBasename($used_local_basename)
    {
        $this->used_local_basename = $used_local_basename;
    }

    public function setRemoteFileSize($remotefilesize)
    {
        if (is_array($remotefilesize)) {
            $this->remotefilesize = end($remotefilesize);
        } else {
            $this->remotefilesize = $remotefilesize;
        }
    }

    public function setLocalFileSize($localfilesize)
    {
        $this->localfilesize = $localfilesize;
    }

    public function setLocalFilePath($path)
    {
        $this->existing_local_path = $path;
    }

    public function setRemoteImgSize($path)
    {
        $remoteimgsize  = getimagesize($path);
        $this->remote_x = $remoteimgsize[0];
        $this->remote_y = $remoteimgsize[1];
    }

    public function setResizeMethod($resizemethod)
    {
        $this->resizemethod = $resizemethod;
    }

    public function setResizeWidth($resizewidth)
    {
        $this->resizewidth = $resizewidth;
    }

    public function setTinyPngToken()
    {
        $this->tinyPngToken = TNYTOKEN;
    }

    public function setIncImgCount($n)
    {
        $this->n++;
        $this->nfmt = str_pad($this->n + 1, 2, 0, STR_PAD_LEFT);
    }

    public function setTiny($tiny)
    {
        $this->tiny = $tiny;
    }

    public function setCompressionsThisMonth($compressions)
    {
        $this->compressions_this_month = $compressions;
    }

    public function makeDirectory($path)
    {
        mkdir("." . $path, "0777", true);
    }

    public function localExists()
    {
        if (empty($this->file_exists)) {
            return false;
        }
        return true;
    }

    public function setFullLocalPath($local_basename = null)
    {
        // this method will loop on a filename to create a new file so that we
        // won't overwrite an existing filename.
        //
        // it also create the directories that are necessary to preserve the
        // path of the optimised image file.
        //
        // use the local-exists.php page to clean a list of images first
        //
        // don't forget this will keep looping until we're happy
        //

        if (empty($local_basename)) {
            // default $this->basename and is then updated as neccessary
            $local_basename = $this->local_basename;
        }
        $full_path = $_SERVER['DOCUMENT_ROOT'] . INSTALL_PATH . $this->outpath . $this->dir . '/' . $local_basename;
        if (file_exists($full_path)) {
            $this->setFileExists();
            $this->setUsedLocalBasename($local_basename);
            $this->setLocalFilePath("."  $this->outpath . $this->dir . '/' . $local_basename);
            if ($this->skip_existing_local) {
                // anything?
            }
            if ($this->overwrite_local_files == false) {
                $this->setIncImgCount($this->n);
                $local_basename = $this->filename . "-" . $this->nfmt . "." . $this->extension;
                $this->setLocalBasename($local_basename);
                $this->setFullLocalPath(); // loop back...
            }
        } else {
            // file doesn't exist locally.
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . INSTALL_PATH . $this->outpath . $this->dir)) {
                $this->makeDirectory($this->outpath . $this->dir);
            }
        }
        $this->full_local_path = "." . $this->outpath . $this->dir . '/' . $local_basename;
    }

} // class Img
