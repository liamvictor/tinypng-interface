<?php

namespace TinyApp;

// HUNT DOWN THE NOUNS
class ImgHTML
{
    // public $path;
    // public $fqdn;
    // public $dir;
    // public $host;
    // public $filename;
    // public $extension;
    // public $basename;
    // public $remotefilepath;
    public $remotefilesize;
    public $remote_x;
    public $remote_y;
    // public $localfilepath;
    public $localfilesize;
    public $local_x;
    public $local_y;
    public $saving;
    public $output;
    public $compare_array;

    // methods
    public function __construct(Img $img)
    {
        try {

            // print("<pre>" . PHP_EOL);
            // var_dump($img);
            // print("</pre>" . PHP_EOL);

            $this->setLocalImgSize($img->full_local_path);
            $this->setRemoteImgSize($img->path);

            $this->getHTML($img);
            //     print("<pre>" . PHP_EOL);
            //     var_dump($this);
            //     var_dump($pathinfo);
            //     var_dump($parsed_url);
            //     print("</pre>" . PHP_EOL);
        } catch (\Exception $e) {
            $this->error[] = $e->getMessage();
        }

    }

    public function setLocalFileSize($path)
    {
        $this->localfilesize = filesize($path);
    }

    public function setLocalImgSize($path)
    {
        if (file_exists($path)) {
            $this->setLocalFileSize($path);
            $localimgsize  = getimagesize($path);
            $this->local_x = $localimgsize[0];
            $this->local_y = $localimgsize[1];
        }
    }

    public function setRemoteFileSize($remotefilesize)
    {
        $this->remotefilesize = $remotefilesize;
    }

    public function setRemoteImgSize($path)
    {
        $remoteimgsize  = getimagesize($path);
        $this->remote_x = $remoteimgsize[0];
        $this->remote_y = $remoteimgsize[1];
    }

    public function getHTML($img)
    {

        if (empty($img->remotefilesize)) {
            $curlObj = new MyCurl($img->path);
            $this->setRemoteFileSize($curlObj->contentLength);
        } else {
            $this->setRemoteFileSize($img->remotefilesize);
        }

        if (!empty($this->remotefilesize) && !empty($this->localfilesize)) {
            $this->saving = $this->remotefilesize - $this->localfilesize;
        } else {
            $this->saving = null;
        }

        $this->compare_array = [
            'Path'           => $img->path,
            'Basename'       => $img->basename,
            'RemoteFileSize' => $this->remotefilesize,
            'RemoteWidth'    => $this->remote_x,
            'RemoteHeight'   => $this->remote_y,
            'LocalFileSize'  => $this->localfilesize,
            'LocalWidth'     => $this->local_x,
            'LocalHeight'    => $this->local_y,
            'Saving'         => $this->saving,
        ];

        $buildOutput = "<tr>";
        $buildOutput .= "<td><a href=\"" . $img->path . "\" target=\"_blank\" rel=\"noopener noreferrer\">" . $img->path . "</a><td>";
        $buildOutput .= "<td>" . number_format($this->remotefilesize) . " bytes</td>";
        $buildOutput .= "<td>" . $this->remote_x . "</td>";
        $buildOutput .= "<td>" . $this->remote_y . "</td>";
        $buildOutput .= "<td>" . number_format($this->localfilesize) . " bytes</td>";
        $buildOutput .= "<td>" . $this->local_x . "</td>";
        $buildOutput .= "<td>" . $this->local_y . "</td>";
        $buildOutput .= "</tr>";
        $this->output = htmlentities($buildOutput);
        // return $this->output;
    }

} // class ImgHTML
