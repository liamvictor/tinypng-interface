<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/../TinyApp/config.php';
echo "<!-- ";
echo __FILE__ . PHP_EOL;
echo INC_ROOT . PHP_EOL;
echo TNYTOKEN . PHP_EOL;
echo "-->" . PHP_EOL;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>TinyPNG Compression Utility Interface</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h1>About This Tool</h1>
    </div>

    <div class="row mt-4">
        <div class="col-xs-12">

<?php

// echo dirname(__DIR__) . "<br />";
// echo $_SERVER['DOCUMENT_ROOT'] . "<br />";

;?>


            <p>To use, enter some URLs or file paths to images on the <a href="tiny-resize.php">resize page</a>.
                The system will use <a href="https://tinypng.com/" target="_blank" rel="noopener noreferrer">TinyPNG's</a> <a href="https://tinypng.com/dashboard/api" target="_blank" rel="noopener noreferrer">API</a> to reduce the file weight, and optionally to reduce the dimentions.</p>

            <h2>Installation</h2>

            <div class="mt-4 mb-4"><code>
                git clone https://liamvictor@bitbucket.org/liamvictor/tinypng-interface.git<br/>
                cd tinypng-interface/<br/>
                composer require tinify/tinify<br/>
                composer dump-autoload -o<br/>
            </code></div>

            <p>You need to copy <code>/TinyApp/config.php.example</code> to <code>/TinyApp/config.php</code> and update it with your own API Token.</p>

            <h2>In Use</h2>

            <p>This system will create a folder in a default folder of <code>./imgs/out</code>, based upon the host name, e.g. <code>./imgs/out/www.example.com</code>. The file structure is maintained by creating the full directory path for each image so that it's easy to upload the new files over the old.</p>

            <p>A remote image of <code>https://www.example.com/path/to/image.jpg</code> will by default be compressed and stored to <code>./imgs/out/www.example.com/path/to/image.jpg</code>.</p>

            <p>If the file name already exists in the target folder you have options to skip, overwrite, or rename it. If renaming then <code>example.jpg</code> will become <code>example-01.jpg</code> or <code>example-02.jpg</code> etc.</p>

            <p>This suite was created by <a href="https://www.onlinesales.co.uk/">Liam Delahunty of OnlineSales Webmaster Tools</a>.</p>

        </div>
    </div>

<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
