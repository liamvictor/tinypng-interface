<?php

require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
require_once INC_ROOT . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Submit the URL(s)
|--------------------------------------------------------------------------
 */

if (isset($_POST['submit']) && isset($_POST['urls'])) {

    if (empty($_POST['urls'])) {
        $errors[] = 'Please enter a url';
        $urls     = null;
    } else {
        $urls = trim($_POST['urls']);
        if (!filter_var($urls, FILTER_SANITIZE_STRING)) {
            // FILTER_SANITIZE_URL allows too much??
            $errors[] = 'Sorry, your input isn\'t a valid string.';
            // filter it!
        }
        $urls = filter_var($urls, FILTER_SANITIZE_STRING);
    }

    if (!empty($urls)) {
        // do it baby
        $url_array = \TinyApp\General::makeArrayAndClean($urls);

        foreach ($url_array as $i => $url) {
            $img = new \TinyApp\Img($url, null, null, null, null, true); // don't check if it's on the server

            // print("<pre>" . PHP_EOL);
            // var_dump($img);
            // print("</pre>" . PHP_EOL);

            if ($img->localExists()) {
                $local_basename = $img->getUsedLocalBasename();
                $local_exists[] = [
                    'path'            => $img->path,
                    'full_local_path' => $img->full_local_path,
                    'basename'        => $img->basename,
                    'remotelink'      => "<a href=\"" . $img->path . "\" target=\"_blank\" rel=\"noopener noreferrer\">" . $img->path . "</a>",
                    'locallink'       => "<a href=\"" . $img->existing_local_path . "\" target=\"_blank\" rel=\"noopener noreferrer\">" . $local_basename . "</a>",
                ];
            } else {
                $remote_only[] = [
                    'path'       => $img->path,
                    'basename'   => $img->basename,
                    'remotelink' => "<a href=\"" . $img->path . "\" rel=\"noopener noreferrer\">" . $img->basename . "</a>",
                ];
            }

            if (!$img->hasErrors()) {
                $imgs[] = $img;
            } else {
                if (is_array($img->error)) {
                    foreach ($img->error as $error) {
                        $errors[] = $error . " " . $img->path;
                    }
                } else {
                    $errors[] = $img->error . " " . $img->path;
                }
            }
            // $alerts[] = $thisimg->basename;

            $url_count = $i + 1;

        }

    }

} else {
    $urls = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Check if Remote File Exists Locally</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h1>Check if Remote File Exists Locally</h1>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-8">

<p>Given some Urls, check if the file exists locally. This is a simple test based on the original file name. It will not catch renamed files.
Report in two columns. This doesn't check the validity of the source.</p>

<?php

if (isset($_POST['submit'])) {
    if (!empty($errors)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
        echo "<ul>";
        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>" . PHP_EOL;
        }
        echo "</ul>";
    }

}

?>


<!--
    /*
    |--------------------------------------------------------------------------
    | FORM
    |--------------------------------------------------------------------------
    */
-->
<form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">
<div class="form-group">
    <label for="urls">Image URLs</label>
    <textarea class="form-control" rows="10" name="urls"><?=$urls;?></textarea>
</div>

<?php
if (!isset($sent)) {
    echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>
        </form>
    </div><!-- col -->
</div><!-- row -->

<?php

if (!empty($remote_only)) {

    ?>

<div class="row mt-4">
    <div class="col-xs-12 col-sm-8">

        <div class="form-group">
            <label for="urls">Remote Only URLs <?=count($remote_only);?> of <?=$url_count;?> URLs input.</label>
            <textarea class="form-control" rows="10" name="urls">

<?php

    foreach ($remote_only as $data) {
        echo $data['path'] . PHP_EOL;
    }

    ?>

            </textarea>
        </div>
    </div><!-- col -->
</div><!-- row -->

<?php

}
; // end remote_only

?>

<?php
/*
|--------------------------------------------------------------------------
$local_exists[] = $img->path;
$remote_only[] = $img->path;
|--------------------------------------------------------------------------
 */
if (isset($local_exists)) {
    if (is_array($local_exists)) {

        echo "<div class=\"row mt-4\"><div class=\"col-xs-12\">";
        echo "<table class=\"table table-striped\"><thead>";
        echo "<tr>";
        echo "<th>Remote</th><th>Local</th>";
        echo "</tr></thead>" . PHP_EOL;
        echo "<tbody>";
        foreach ($local_exists as $key => $row) {
            echo "<tr>";
            echo "<td>" . $row['remotelink'] . "</td>";
            echo "<td>" . $row['locallink'] . "</td>";
            echo "</tr>" . PHP_EOL;
        }
        echo "</tbody></table>" . PHP_EOL;

        echo "<p>Local " . count($local_exists) . " of " . $url_count . " URLs entered.</p>";

        echo "</div><!-- col --></div><!-- row -->" . PHP_EOL;
    }
}

?>

<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
