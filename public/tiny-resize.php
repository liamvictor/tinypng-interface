<?php

require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
require_once INC_ROOT . '/vendor/autoload.php';

// defaults

$c                     = 0;
$width                 = 0;
$method                = "scale";
$overwrite_local_files = 0;
$skip_existing_local   = 1;

/*
|--------------------------------------------------------------------------
| Submit the URL(s)
|--------------------------------------------------------------------------
 */

if (isset($_POST['submit']) && isset($_POST['urls'])) {

    // print("<pre>" . PHP_EOL);
    // var_dump($_POST);
    // print("</pre>" . PHP_EOL);

    if (!empty($_POST['width'])) {
        $width = filter_var($_POST['width'], FILTER_SANITIZE_NUMBER_INT);
    }

    if (!empty($_POST['method'])) {
        // need to see if it is one of the options: fit, scale, cover, thumb
        $method = filter_var($_POST['method'], FILTER_SANITIZE_STRING);
    }

    if (!empty($_POST['overwrite_local_files'])) {
        // default is to overwrite_local_files
        $overwrite_local_files = filter_var($_POST['overwrite_local_files'], FILTER_SANITIZE_STRING);
    } else {
        $overwrite_local_files = 0;
    }

    if (!empty($_POST['skip_existing_local'])) {
        // default is to skip.
        $skip_existing_local = filter_var($_POST['skip_existing_local'], FILTER_SANITIZE_STRING);
    } else {
        $skip_existing_local = 0;
    }

    if (empty($_POST['urls'])) {
        $errors[] = 'Please enter a url';
        $urls     = null;
    } else {
        $urls = trim($_POST['urls']);
        if (!filter_var($urls, FILTER_SANITIZE_STRING)) {
            // FILTER_SANITIZE_URL allows too much??
            $errors[] = 'Sorry, your input isn\'t a valid string.';
            // filter it!
        }
        $urls = filter_var($urls, FILTER_SANITIZE_STRING);
    }

    if (!empty($urls)) {
        // do it baby
        $general   = new \TinyApp\General();
        $url_array = $general->makeArrayAndClean($urls);
        if (count($url_array) > $max_img_upload_limit) {
            $errors[] = "Sorry, there's more than $max_img_upload_limit URLs. ";
        } else {
            $start_preprocessing = microtime(true);

            foreach ($url_array as $i => $url) {
                $img = new \TinyApp\Img($url, $method, $width, $overwrite_local_files, $skip_existing_local);

                if (!$img->hasErrors()) {
                    $imgs[] = $img;
                } else {
                    if (is_array($img->error)) {
                        foreach ($img->error as $error) {
                            $errors[] = $error . " " . $img->path;
                        }
                    } else {
                        $errors[] = $img->error . " " . $img->path;
                    }
                }
                // $alerts[] = $thisimg->basename;
            }

            $end_preprocessing_time = microtime(true);
            $preprocessing_len      = $end_preprocessing_time - $start_preprocessing;
            // add one.
            $i++;
            $alerts[] = "<b>Preprocessing</b> $i images complete in $preprocessing_len microtime.";

            if (!empty($imgs)) {
                $queue = new \TinyApp\MakeImgQueue($imgs);
            }

        }

        if (!empty($queue->queue_item)) {

            if (count($queue->queue_item) > $img_upload_limit) {
                $errors[] = "Sorry, there's more than $img_upload_limit URLs (" . count($queue->queue_item) . " fresh URLs in quete from " . count($url_array) . "). We're going to quit processing after $img_upload_limit files. ";
            }

            $alerts[] = count($queue->queue_item) . " fresh items from " . count($url_array) . " URLs submitted.";

            foreach ($queue->queue_item as $i => $img) {
                set_time_limit(30);
                // this calls the Tinify stuff which performs the compression
                // and downloads the file

                $item = new \TinyApp\TinyInterface($img);

                if (!empty($img->error)) {
                    break;
                }

/*
|--------------------------------------------------------------------------
| $c is after the error test otherwise the processed number is wrong.
|--------------------------------------------------------------------------
 */
                $c           = $i + 1;
                $processed[] = $item;
                $report[]    = "<tr><td><a href=\"$img->path\" target=\"_blank\">" . $img->basename . "</a></td>
                                <td><a href=\"$img->full_local_path\" target=\"_blank\">" . $img->local_basename . "</a></td>
                                <td>" . $img->remotefilesize . "</td>
                                <td>" . $img->response . "</td>
                                </tr>";

                if ($c == $img_upload_limit) {
                    break;
                }
            }
            $end_processing_time = microtime(true);
            $processing_len      = $end_processing_time - $end_preprocessing_time;
            if (!empty($img->compressions_this_month)) {
                $alerts[] = "Compressions this month: " . $img->compressions_this_month;
            }
            $alerts[] = "<b>Complete</b> Processed " . $c . " images complete in $processing_len microtime.";

        } else {
            $alerts[] = "<b>Complete - Nothing to process.</b>";
        }
    }

    if (!empty($img->error)) {
        foreach ($img->error as $error) {
            $errors[] = $error;
        }
    }

} else {
    $urls = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Resize and Reduce Images with TinyPng</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h2>Reduce Image Weight &amp; Resize</h2>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-8">
          <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">

<?php

if (isset($_POST['submit'])) {

    if (!empty($errors)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
        echo "<ul>";
        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>" . PHP_EOL;
        }
        echo "</ul>";
    }

    if (!empty($alerts)) {
        echo "<div class=\"alert alert-success\" role=\"alert\">Processing:</div>";

        echo "<ul>" . PHP_EOL;
        foreach ($alerts as $alert) {
            echo "<li>$alert</li>" . PHP_EOL;
        }
        echo "</ul>" . PHP_EOL;

        if (!empty($report)) {
            echo "<table class=\"table table-striped\">" . PHP_EOL;
            echo "<thead><tr><th>Source</th><th>Local</th><th>Remote Size</th><th>Response</th></tr></thead>" . PHP_EOL;
            echo "<tbody>" . PHP_EOL;
            foreach ($report as $line) {
                echo $line . PHP_EOL;
            }
            echo "</tbody>" . PHP_EOL;
            echo "</table>";
        }

        // if (!empty($processed)) {
        //     echo "<pre>processed";
        //     var_dump($processed);
        //     echo "</pre>";
        // }
    }
}

?>

<div class="form-group">
    <label for="urls">Source Images<br /><small>URLs <code>https://example.com/path/to/file</code> or local paths <code>/path/to/file</code> <?=$max_img_upload_limit;?> max files input - note <?=$img_upload_limit;?> files can be processed at once.</small></label>
    <textarea class="form-control" rows="10" name="urls"><?=$urls;?></textarea>
</div>

<div class="form-group">
    <label for="width">Width Limit<br /><small>(0 not to resize)</small></label>
    <input type="text" class="form-control" name="width" id="width" placeholder="Please enter maximum width" value="<?=$width;?>">
</div>

<p>Consider how these two options can clash:</p>

<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="skip_existing_local" name="skip_existing_local"
<?php
if (!empty($_POST['skip_existing_local']) || !empty($skip_existing_local)) {
    echo " checked";
}
?>
    >
    <label class="form-check-label" for="skip_existing_local">Skip Existing local file (doesn't process images already saved locally)</label>
</div>


<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="overwrite_local_files" name="overwrite_local_files"
<?php
if (!empty($_POST['overwrite_local_files']) || !empty($overwrite_local_files)) {
    echo " checked";
}
?>
    >
    <label class="form-check-label" for="overwrite_local_files">Overwrite local file (when unchecked will create a new file if not skipping)</label>
</div>


<?php
if (!isset($sent)) {
    echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>
        </form>
    </div><!-- col -->
</div><!-- row -->

<div class="row mt-4">
    <div class="col-xs-12 col-sm-8">
        <p><a href="make-img-comparison-page.php">Make image comparison page</a>.</p>
    </div><!-- col -->
</div><!-- row -->

<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
