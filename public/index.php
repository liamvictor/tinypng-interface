<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>TinyPNG Compression Utility</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h1>TinyPNG API Wondrousness</h1>
    </div>

    <div class="row mt-4">
        <div class="col-xs-12">
            <ul>
                <li><a href="tiny-resize.php">Compress Images &amp; Resize </a></li>
                <li><a href="img-data.php">Get Image Data for Urls.</a></li>
            </ul>

            <p>I wrote this to solve the problem of reducing many large images discovered during a <a href="https://www.screamingfrog.co.uk/seo-spider/" rel="noopener noreferrer">Screaming Frog Spider</a> crawl and then having to painstakingly upload the compressed images one-by-one. This system allows you to enter URLs of images, and then saves the compressed images with the same file structure. I can then upload all the compressed and resized images in one pass.</p>

            <p>To use, enter some URLs or file paths to JPG or PNG images on the <a href="tiny-resize.php">resize page</a>. The system will use <a href="https://tinypng.com/" rel="noopener noreferrer">TinyPNG's</a> PHP API to reduce the file weight, and optionally to reduce the dimentions. If the file name already exists in the target folder you have options to skip it, overwrite it, or rename it.</p>

            <p>This suite was created by <a href="https://www.onlinesales.co.uk/"  target="_blank">Liam Delahunty of OnlineSales</a>.</p>

        </div>
    </div>

<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
