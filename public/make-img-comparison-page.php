<?php

require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
require_once INC_ROOT . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Submit the URL(s)
|--------------------------------------------------------------------------
 */

if (isset($_POST['submit']) && isset($_POST['urls'])) {
    if (empty($_POST['urls'])) {
        $errors[] = 'Please enter a url';
        $urls     = null;
    } else {
        $urls = trim($_POST['urls']);
        if (!filter_var($urls, FILTER_SANITIZE_STRING)) {
            // FILTER_SANITIZE_URL allows too much??
            $errors[] = 'Sorry, your urls aren\'t a valid string.';
            // filter it!
        }
        $urls = filter_var($urls, FILTER_SANITIZE_STRING);
    }

    if (!empty($urls)) {
        // do it baby
        $url_array = TinyApp\General::makeArrayAndClean($urls);
    }

    if (!empty($url_array)) {

        foreach ($url_array as $i => $url) {
            $img = new \TinyApp\Img($url, null, null, true, true);
            // print("<pre>" . PHP_EOL);
            // var_dump($img);
            // print("</pre>" . PHP_EOL);
            if (!$img->hasErrors()) {

                $imgs[] = $img;
            } else {
                if (is_array($img->error)) {
                    foreach ($img->error as $error) {
                        $errors[] = $error . " " . $img->path;
                    }
                } else {
                    $errors[] = $img->error . " " . $img->path;
                }
            }
        }

        if (!empty($imgs)) {
            foreach ($imgs as $key => $img) {
                $imghtml         = new \TinyApp\ImgHTML($img);
                $compare_array[] = $imghtml->compare_array;
            }
        }

    }

} else {
    $urls = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Make Img Comparison Page</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h2>Make Image Comparison Page</h2>
        <p>Please enter the URLs of the source images.</p>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-8">
          <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">

<?php

if (isset($_POST['submit'])) {
    if (!empty($errors)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
        echo "<ul>";
        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>" . PHP_EOL;
        }
        echo "</ul>";
    }
}

?>

<div class="form-group">
    <label for="urls">Image URLs</label>
    <textarea class="form-control" rows="10" name="urls"><?=$urls;?></textarea>
</div>

<?php
if (!isset($sent)) {
    echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>



        </form>
    </div><!-- col -->
</div><!-- row -->



<?php

if (!empty($compare_array)) {
    echo "<div class=\"row mt-4\"><div class=\"col-sm-8\"><textarea class=\"form-control\" rows=\"10\">";
    foreach ($compare_array as $row) {
        foreach ($row as $item) {
            echo $item . "\t";
        }
        echo PHP_EOL;
    }
    echo "</textarea></div></div>" . PHP_EOL;
}

if (isset($compare_array)) {
    if (is_array($compare_array)) {
        $txt = null;
        echo "<div class=\"row mt-4\"><div class=\"col-xs-12\">";
        echo "<table class=\"table table-striped\"><thead>";
        echo "<tr><th>";
        echo implode('</th><th>', array_keys(current($compare_array)));
        echo "</th></tr></thead>" . PHP_EOL;
        echo "<tbody>";
        foreach ($compare_array as $row): array_map('htmlentities', $row);
            echo "<tr><td>";
            echo implode('</td><td>', $row);
            echo "</td></tr>";
        endforeach;
        echo "</tbody></table>" . PHP_EOL;
        echo "</div><!-- col --></div><!-- row -->" . PHP_EOL;
    }
}

if (isset($myoutput)) {
    foreach ($myoutput as $row) {
        echo "<div class=\"row\" style=\"border-bottom:1px solid #ccc; margin-bottom 2em; padding:1em 0px;\">" . $row . "</div>" . PHP_EOL;
    }
}

?>


<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
