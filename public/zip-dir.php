<?php

require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
require_once INC_ROOT . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Submit the URL(s)
|--------------------------------------------------------------------------
 */

if (isset($_POST['submit'])) {

    if (!empty($_POST['dir'])) {
        $dir = filter_var($_POST['dir'], FILTER_SANITIZE_STRING);
    } else {
        $dir      = null;
        $errors[] = "Please enter a directory name.";
    }

    // if (!empty($_POST['overwrite_old_zip'])) {
    //     // default is to overwrite_local_files
    //     $overwrite_old_zip = filter_var($_POST['overwrite_old_zip'], FILTER_SANITIZE_STRING);
    // } else {
    //     $overwrite_old_zip = 0;
    // }

    $source      = $_SERVER['DOCUMENT_ROOT'] . INSTALL_PATH . OUTPATH_ROOT . $dir;
    $destination = $_SERVER['DOCUMENT_ROOT'] . INSTALL_PATH . OUTPATH_ROOT;

    if (is_dir($source)) {

        // $alerts[] = "<a href=\"$source\">$source</a> is  a valid directory.";

        $zip_name    = preg_replace('/[^a-z0-9]+/', '-', strtolower($dir)) . ".zip";
        $destination = $destination . $zip_name;
        $link        = INSTALL_PATH . OUTPATH_ROOT . $zip_name;

        $general = new \TinyApp\General();
        if ($general->zipData($source, $destination) == true) {
            $alerts[] = "Zipped: <a href=\"$link\">$zip_name</a>.";
        }

    } else {
        $errors[] = "$source is not a valid directory.";
    }
} else {
    $dir = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Zip up directory</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h1>Zip Directory</h1>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-8">

<?php

if (isset($_POST['submit'])) {

    if (!empty($errors)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
        echo "<ul>";
        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>" . PHP_EOL;
        }
        echo "</ul>";
    }

    if (!empty($alerts)) {
        echo "<div class=\"alert alert-success\" role=\"alert\">Processing:</div>";

        echo "<ul>" . PHP_EOL;
        foreach ($alerts as $alert) {
            echo "<li>$alert</li>" . PHP_EOL;
        }
        echo "</ul>" . PHP_EOL;

    }
}

?>

<form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">

<div class="form-group">
    <label for="dir">Dir path <br /><small>(within <code><?=INSTALL_PATH . OUTPATH_ROOT;?></code>)</small></label>
    <input type="text" class="form-control" name="dir" id="dir" placeholder="Please enter your directory to zip" value="<?=$dir;?>">
</div>

<?php
/*
|--------------------------------------------------------------------------
| Commented out for now.
| Perhaps one day...
|--------------------------------------------------------------------------
<div class="form-group form-check">
<input type="checkbox" class="form-check-input" id="overwrite_old_zip" name="overwrite_old_zip"
if (!empty($_POST['overwrite_old_zip']) || !empty($overwrite_old_zip)) {
echo " checked";
}
>
<label class="form-check-label" for="overwrite_old_zip">Overwrite Existing Zip File (when unchecked will create a new file)</label>
</div>
 */
;?>


<?php
if (!isset($sent)) {
    echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>
        </form>
    </div><!-- col -->
</div><!-- row -->


<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
