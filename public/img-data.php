<?php

require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/TinyApp/config.php';
require_once INC_ROOT . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Submit the URL(s)
|--------------------------------------------------------------------------
 */

if (isset($_POST['submit']) && isset($_POST['urls'])) {
    if (empty($_POST['urls'])) {
        $errors[] = 'Please enter a url';
        $urls     = null;
    } else {
        $urls = trim($_POST['urls']);
        if (!filter_var($urls, FILTER_SANITIZE_STRING)) {
            // FILTER_SANITIZE_URL allows too much??
            $errors[] = 'Sorry, your urls isn\'t a valid string.';
            // filter it!
        }
        $urls = filter_var($urls, FILTER_SANITIZE_STRING);
    }

    if (!empty($urls)) {
        // do it baby
        $url_array = \TinyApp\General::makeArrayAndClean($urls);
    }

    if (!empty($url_array)) {

        foreach ($url_array as $i => $url) {
            $img = new \TinyApp\Img($url, null, null, true, true);
            if (!$img->hasErrors()) {
                $imgs[] = $img;
            } else {
                if (is_array($img->error)) {
                    foreach ($img->error as $error) {
                        $errors[] = $error . " " . $img->path;
                    }
                } else {
                    $errors[] = $img->error . " " . $img->path;
                }
            }
        }

        if (!empty($imgs)) {
            foreach ($imgs as $key => $img) {
                $imghtml         = new \TinyApp\ImgHTML($img);
                $compare_array[] = $imghtml->compare_array;
            }
        }

    }

    if (!empty($compare_array)) {
        $compare_keys   = array_keys(current($compare_array));
        $compare_counts = [];
        foreach ($compare_keys as $compare_key) {
            # code...
            foreach ($compare_array as $compare_row) {
                if (!empty($compare_row[$compare_key])) {
                    if (array_key_exists($compare_key, $compare_counts)) {
                        $compare_counts[$compare_key]++;
                    } else {
                        $compare_counts[$compare_key] = 1;
                    }
                }
            }
        }

        // print("<pre>" . PHP_EOL);
        // var_dump($compare_counts);
        // print("</pre>" . PHP_EOL);

    }
} else {
    $urls = null;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Get Remote Image Data</title>
<?php include INC_ROOT . '/TinyApp/page/css.php';?>
</head>
<body>

<?php include INC_ROOT . '/TinyApp/page/header-nav.php';?>

<div class="container" role="main">


    <div class="header">
        <h1>Get Img Data</h1>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-8">

<p>Enter some Urls or local paths to images. This will gather the data on the images.
If there is a local and remote image then we show the data for each. The local image path is based upon the remote's URL. See <a href="./about.php">about</a> for more information.</p>

          <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">

<?php

if (isset($_POST['submit'])) {
    if (!empty($errors)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
        echo "<ul>";
        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>" . PHP_EOL;
        }
        echo "</ul>";
    }
}

?>

<div class="form-group">
    <label for="urls">Image URLs</label>
    <textarea class="form-control" rows="10" name="urls"><?=$urls;?></textarea>
</div>

<?php
if (!isset($sent)) {
    echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>
        </form>
    </div><!-- col -->
</div><!-- row -->


<?php

if (!empty($compare_array)) {
    echo "<div class=\"row mt-4\"><div class=\"col-sm-8\"><textarea class=\"form-control\" rows=\"10\">";
    foreach ($compare_array as $row) {
        foreach ($row as $item) {
            echo $item . "\t";
        }
        echo PHP_EOL;
    }
    echo "</textarea></div></div>" . PHP_EOL;
}

if (isset($compare_array)) {
    if (is_array($compare_array)) {

        echo "<div class=\"row mt-4\"><div class=\"col-xs-12\">";
        echo "<table class=\"table table-striped\"><thead>";
        echo "<tr>";
        foreach (array_keys($compare_counts) as $key) {
            echo "<th>$key</th>";
        }
        echo "</tr></thead>" . PHP_EOL;
        echo "<tbody>";
        foreach ($compare_array as $key => $row) {
            echo "<tr>";
            foreach (array_keys($compare_counts) as $key) {
                echo "<td>" . $row[$key] . "</td>";
            }
            echo "</tr>" . PHP_EOL;
        }
        echo "</tbody></table>" . PHP_EOL;
        echo "</div><!-- col --></div><!-- row -->" . PHP_EOL;
    }
}

if (isset($myoutput)) {
    foreach ($myoutput as $row) {
        echo "<div class=\"row\" style=\"border-bottom:1px solid #ccc; margin-bottom 2em; padding:1em 0px;\">" . $row . "</div>" . PHP_EOL;
    }
}

?>

<?php include INC_ROOT . '/TinyApp/page/footer.php';?>

        </div> <!-- /container -->

<?php include INC_ROOT . '/TinyApp/page/footer-includes.php';?>

    </body>
</html>
